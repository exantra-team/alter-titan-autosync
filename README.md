# Alter Titan AutoSync

## How to set up the development environment

1. Install NodeJS (version 10 or newer) and NPM if you haven't
2. Fork this project
3. Clone your fork
4. Go to the project folder and install modules with command: `npm install`
5. Install VSCode https://code.visualstudio.com/
6. Open the project folder with VSCode
7. Install VSCode extension [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) and [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
8. Open `index.js`
9. Check the bottom-right corner in VSCode and make sure ESLint is running.

## How to implement a new provider

Create a new javascript file in `auto-sync-providers` folder, then create a class extends AutoSyncProvider. I've created a template `Template.js`, you can duplicate the file then change the file name and class name.

**Google Fit** is implemented so you can take it as an example. You can test it out by runing the command `npm start`

If you want to test the new provider, go to `index.js` and add the new provider and return a new instance of the provider class in the function `getAuthSyncProvider(name)`. You can take **Google Fit** as an example. Also you'll probably need to add a url `http://localhost:8000/auth-callback` to the new provider's callback whitelist.
