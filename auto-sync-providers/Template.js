const {
  default: AutoSyncProvider,
  AutoSyncError,
} = require("./AutoSyncProvider");

class Template extends AutoSyncProvider {
  /**
   * Called when setting up for the first time.
   * @returns {string} Returns the OAuth URL.
   */
  setup() {
    throw new AutoSyncError("UNSUPPORTED");
  }

  /**
   * Called after the OAuth authentication
   * @param {string} search The URL query strings in the callback. example: ?code=123456&scope=asdf
   * @returns {Promise<string>} Returns a Promise of a token string which is used for future syncs. For example, Google uses Refresh Token.
   */
  authCallback(search) {
    throw new AutoSyncError("UNSUPPORTED");
  }

  /**
   * Pulls the workout and step data from the provider's API with the time provided.
   * @param {string} authToken The token being used for authentication
   * @param {number} workoutStartTime Unix Epoch of the start-time for workout
   * @param {number} stepStartTime Unix Epoch of the start-time for steps
   * @returns {Promise<{workouts: {startTime: number, duration: number, type: string}[], steps: {endTime: number, count: number}}>} Returns a Promise of an object which contains an array of workout details and total step counts. startTime and endTime are Unix Epoch.
   */
  sync(authToken, workoutStartTime, stepStartTime) {
    throw new AutoSyncError("UNSUPPORTED");
  }
}

exports.default = Template;