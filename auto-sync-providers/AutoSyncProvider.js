class AutoSyncProvider {
  /**
   * @returns {"CLIENT" | "PULL" | "PUSH"}
   */
  getSyncMethod() {
    throw new AutoSyncError("UNSUPPORTED");
  }

  /**
   * Called when setting up for the first time.
   * @returns {Promise<{url: string, secret: string}?>} Returns the OAuth URL. Returns null if server-side API is not avaliable.
   */
  setup() {
    throw new AutoSyncError("UNSUPPORTED");
  }

  /**
   * Called after the OAuth authentication
   * @param {string} search The URL query strings in the callback. example: ?code=123456&scope=asdf
   * @param {string} secret The secret returned from setup method
   * @returns {Promise<string>} Returns a Promise of a token string which is used for future syncs. For example, Google uses Refresh Token.
   */
  authCallback(search, secret) {
    throw new AutoSyncError("UNSUPPORTED");
  }

  /**
   * Called after successful auth.
   * @returns {Promise<string>?} Return initial save if save is used in sync.
   */
  postAuthSave() {
    return null;
  }

  /**
   * Called before sync. Try to save the access token as a property for the "sync" method.
   * @param {string} authToken The auth token returned by authCallback
   * @returns {Promise<string?>} Returns a new token string if required.
   */
  setAuthToken(authToken) {
    throw new AutoSyncError("UNSUPPORTED");
  }

  /**
   * @typedef AutoSyncResult
   * @type {{workouts: {data: {startTime: number, duration: number, type: string}[], endTime: number}, steps: {count: number, endTime: number}, save: string?}}
   */

  /**
   * Pulls the workout and step data from the provider's API with the time provided.
   * @param {{
   * workoutStartTime: number
   * stepStartTime: number
   * save: string?
   * }} parameters
   * workoutStartTime: Unix Epoch of the start-time for workout
   *
   * stepStartTime: Unix Epoch of the start-time for steps
   *
   * save: The last returned save
   *
   * @returns {Promise<AutoSyncResult>} Returns a Promise of an object which contains an array of workout details and total step counts. startTime and endTime are Unix Epoch.
   */
  sync(parameters) {
    throw new AutoSyncError("UNSUPPORTED");
  }

  /**
   * Called only when client-side API is used. No need to implement this if server-side API is available.
   * @returns {Promise<AutoSyncResult>}
   */
  syncFromClient(data) {
    throw new AutoSyncError("UNSUPPORTED");
  }

  /**
   * Called only if the provider server pushes data. Only requred when using PUSH method. e.g.: Garmin
   * @returns {AutoSyncResult}
   */
  syncFromPush(data) {
    throw new AutoSyncError("UNSUPPORTED");
  }

  /**
   * Get the id for the pushed data. Only requred when using PUSH method.
   * @param {string} authToken The auth token returned by authCallback
   * @returns {Promise<string?>} Returns the id.
   */
  getPushId(authToken) {
    throw new AutoSyncError("UNSUPPORTED");
  }

  /**
   * Called when the user request to unregister.
   * @param {string} authToken The auth token returned by authCallback
   * @returns {Promise}
   */
  deregister(authToken) {
    throw new AutoSyncError("UNSUPPORTED");
  }
}

/**
 * @typedef AutoSyncErrorCode
 * @type {"REQUIRE_REAUTHORIZATION" | "AUTHORIZATION_FAILED" | "UNSUPPORTED"}
 */

class AutoSyncError extends Error {
  /**
   * @param {AutoSyncErrorCode} code
   * @param {string?} message
   */
  constructor(code, message) {
    super(message);
    this.code = code;
  }
}

exports.default = AutoSyncProvider;
exports.AutoSyncError = AutoSyncError;
