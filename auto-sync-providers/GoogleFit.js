const {
  default: AutoSyncProvider,
  AutoSyncError,
} = require("./AutoSyncProvider");
const { google } = require("googleapis");

class GoogleFit extends AutoSyncProvider {
  /** @param {{clientId: string, clientSecret: string, redirectUri: string}} config */
  constructor(config) {
    super();
    this._client = new google.auth.OAuth2(config);
  }

  getSyncMethod() {
    return "PULL";
  }

  setup() {
    return {
      url: this._client.generateAuthUrl({
        access_type: "offline",
        scope: ["https://www.googleapis.com/auth/fitness.activity.read"],
      }),
    };
  }

  authCallback(search) {
    return new Promise((resolve, reject) => {
      const urlParams = new URLSearchParams(search);

      this._client.on("tokens", (tokens) => {
        if (tokens.refresh_token) {
          resolve(tokens.refresh_token);
        } else {
          this._client.revokeToken(tokens.access_token);
          reject(new AutoSyncError("REQUIRE_REAUTHORIZATION"));
        }
      });

      const code = urlParams.get("code");
      this._client.getToken(code);
    });
  }

  setAuthToken(authToken) {
    return new Promise((resolve) => {
      this._client.setCredentials({ refresh_token: authToken });
      resolve();
    });
  }

  async sync({ workoutStartTime, stepStartTime }) {
    const fit = new GoogleFitClient(this._client);
    const workoutStartDate = new Date(workoutStartTime);
    const stepStartDate = new Date(stepStartTime);
    const endDate = new Date();
    try {
      const [activities, steps, calories] = await Promise.all([
        fit.getActivity(workoutStartDate, endDate),
        fit.getSteps(stepStartDate, endDate),
        fit.getCalories(workoutStartDate, endDate),
      ]);

      const nanoSecondsPerMillisecond = 1000000;

      const caloriesMap = calories.point.reduce(
        (obj, { value, startTimeNanos, endTimeNanos }) => {
          obj[`${startTimeNanos}-${endTimeNanos}`] = value[0].fpVal;
          return obj;
        },
        {}
      );

      const activityResults = activities.point
        .map(({ value, startTimeNanos, endTimeNanos }) => {
          const startTimeMs = Math.floor(
            parseInt(startTimeNanos) / nanoSecondsPerMillisecond
          );
          const endTimeMs = Math.floor(
            parseInt(endTimeNanos) / nanoSecondsPerMillisecond
          );
          const activity = fit.getActivityDescription(value[0].intVal);
          return {
            startTime: startTimeMs,
            duration: endTimeMs - startTimeMs,
            calories: caloriesMap[`${startTimeNanos}-${endTimeNanos}`],
            type: activity,
          };
        })
        .filter((e) => e.type);

      const stepResults = steps.point
        .map(({ value, startTimeNanos, endTimeNanos, originDataSourceId }) => {
          const startTimeMs = Math.floor(
            parseInt(startTimeNanos) / nanoSecondsPerMillisecond
          );
          const endTimeMs = Math.floor(
            parseInt(endTimeNanos) / nanoSecondsPerMillisecond
          );
          return {
            endTime: endTimeMs,
            count: value[0].intVal,
          };
        })
        .reduce(
          (obj, e) => ({
            count: obj.count + e.count,
            endTime: Math.max(obj.endTime, e.endTime),
          }),
          { count: 0, endTime: 0 }
        );

      return {
        workouts: {
          data: activityResults,
          endTime: activityResults.reduce((t, e) => {
            const endTime = e.startTime + e.duration;
            return endTime > t ? endTime : t;
          }, 0),
        },
        steps: stepResults,
      };
    } catch (e) {
      console.error(e);
      if (e.code === "400") {
        throw new AutoSyncError("REQUIRE_REAUTHORIZATION");
      }

      throw e;
    }
  }
}

class GoogleFitClient {
  constructor(auth) {
    google.options({ auth });

    /*
     * A map of activity type names to activity code.
     * @const
     * @see {@link https://developers.google.com/fit/rest/v1/reference/activity-types|Google Fit activity types}
     */
    this.ACTIVITY_TYPE = {
      // IN_VEHICLE: 0,
      BIKING: 1,
      // ON_FOOT: 2,
      // "STILL (NOT MOVING)": 3,
      // "UNKNOWN (UNABLE TO DETECT ACTIVITY)": 4,
      // "TILTING (SUDDEN DEVICE GRAVITY CHANGE)": 5,
      // WALKING: 7,
      RUNNING: 8,
      AEROBICS: 9,
      BADMINTON: 10,
      BASEBALL: 11,
      BASKETBALL: 12,
      BIATHLON: 13,
      HANDBIKING: 14,
      MOUNTAIN_BIKING: 15,
      ROAD_BIKING: 16,
      SPINNING: 17,
      STATIONARY_BIKING: 18,
      UTILITY_BIKING: 19,
      BOXING: 20,
      CALISTHENICS: 21,
      CIRCUIT_TRAINING: 22,
      CRICKET: 23,
      CURLING: 106,
      DANCING: 24,
      DIVING: 102,
      ELLIPTICAL: 25,
      ERGOMETER: 103,
      FENCING: 26,
      "FOOTBALL (AMERICAN)": 27,
      "FOOTBALL (AUSTRALIAN)": 28,
      "FOOTBALL (SOCCER)": 29,
      FRISBEE: 30,
      GARDENING: 31,
      GOLF: 32,
      GYMNASTICS: 33,
      HANDBALL: 34,
      HIKING: 35,
      HOCKEY: 36,
      HORSEBACK_RIDING: 37,
      HOUSEWORK: 38,
      ICE_SKATING: 104,
      JUMPING_ROPE: 39,
      KAYAKING: 40,
      KETTLEBELL_TRAINING: 41,
      KICKBOXING: 42,
      KITESURFING: 43,
      MARTIAL_ARTS: 44,
      MEDITATION: 45,
      MIXED_MARTIAL_ARTS: 46,
      P90X_EXERCISES: 47,
      PARAGLIDING: 48,
      PILATES: 49,
      POLO: 50,
      RACQUETBALL: 51,
      ROCK_CLIMBING: 52,
      ROWING: 53,
      ROWING_MACHINE: 54,
      RUGBY: 55,
      JOGGING: 56,
      RUNNING_ON_SAND: 57,
      "RUNNING (TREADMILL)": 58,
      SAILING: 59,
      SCUBA_DIVING: 60,
      SKATEBOARDING: 61,
      SKATING: 62,
      CROSS_SKATING: 63,
      INDOOR_SKATING: 105,
      "INLINE_SKATING (ROLLERBLADING)": 64,
      SKIING: 65,
      "BACK-COUNTRY SKIING": 66,
      "CROSS-COUNTRY SKIING": 67,
      DOWNHILL_SKIING: 68,
      KITE_SKIING: 69,
      ROLLER_SKIING: 70,
      SLEDDING: 71,
      // SLEEPING: 72,
      // LIGHT_SLEEP: 109,
      // DEEP_SLEEP: 110,
      // REM_SLEEP: 111,
      // "AWAKE (DURING SLEEP CYCLE)": 112,
      SNOWBOARDING: 73,
      SNOWMOBILE: 74,
      SNOWSHOEING: 75,
      SQUASH: 76,
      STAIR_CLIMBING: 77,
      "STAIR-CLIMBING MACHINE": 78,
      "STAND-UP PADDLEBOARDING": 79,
      STRENGTH_TRAINING: 80,
      SURFING: 81,
      SWIMMING: 82,
      "SWIMMING (OPEN WATER)": 84,
      "SWIMMING (SWIMMING POOL)": 83,
      "TABLE TENNIS (PING PONG)": 85,
      TEAM_SPORTS: 86,
      TENNIS: 87,
      "TREADMILL (WALKING OR RUNNING)": 88,
      VOLLEYBALL: 89,
      "VOLLEYBALL (BEACH)": 90,
      "VOLLEYBALL (INDOOR)": 91,
      WAKEBOARDING: 92,
      "WALKING (FITNESS)": 93,
      NORDING_WALKING: 94,
      "WALKING (TREADMILL)": 95,
      WATERPOLO: 96,
      WEIGHTLIFTING: 97,
      WHEELCHAIR: 98,
      WINDSURFING: 99,
      YOGA: 100,
      ZUMBA: 101,

      "OTHER (UNCLASSIFIED FITNESS ACTIVITY)": 108,
    };

    // Create a reverse map of activity codes to name.
    this.ACTIVITY_MAP = {};
    for (const [key, value] of Object.entries(this.ACTIVITY_TYPE)) {
      this.ACTIVITY_MAP[value] = key;
    }

    this.NINETY_DAYS_MS = 90 * 24 * 60 * 60 * 1000;
    this.ONE_DAY_MS = 24 * 60 * 60 * 1000;
    this._fitness = google.fitness("v1").users;
  }

  /**
   * Gets the description for the activity given the activity ID.
   * @param {number} The activity ID
   * @return {string} The activity description
   */
  getActivityDescription(id) {
    return this.ACTIVITY_MAP[id];
  }

  /**
   * Gets activity for the signed in user during the given time period.
   *
   * @param {Date} startTime the start time for the period to get activity
   * @param {Date} endTime the end time for the period to get activity
   */
  getActivity(startTime, endTime) {
    return this._getDatasets(
      "derived:com.google.activity.segment:com.google.android.gms:merge_activity_segments",
      startTime,
      endTime
    );
  }

  /**
   * Gets activity for the signed in user during the given time period.
   *
   * @param {Date} startTime the start time for the period to get activity
   * @param {Date} endTime the end time for the period to get activity
   */
  getCalories(startTime, endTime) {
    return this._getDatasets(
      "derived:com.google.calories.expended:com.google.android.gms:merge_calories_expended",
      startTime,
      endTime
    );
  }

  /**
   * Gets steps for the signed in user during the given time period.
   *
   * @param {Date} startTime the start time for the period to get steps
   * @param {Date} endTime the end time for the period to get steps
   */
  getSteps(startTime, endTime) {
    return this._getDatasets(
      "derived:com.google.step_count.delta:com.google.android.gms:merge_step_deltas",
      startTime,
      endTime
    );
  }

  /**
   * Gets weight for the signed in user during the given time period.
   *
   * @param {Date} startTime the start time for the period to get weight
   * @param {Date} endTime the end time for the period to get weight
   */
  getWeight(startTime, endTime) {
    return this._getDatasets(
      "derived:com.google.weight:com.google.android.gms:merge_weight",
      startTime,
      endTime
    );
  }

  /**
   * Gets heart rate for the signed in user during the given time period.
   * @param {Date} startTime the start time for the period to get weight
   * @param {Date} endTime the end time for the period to get weight
   */
  getHeartRate(startTime, endTime) {
    return this._getDatasets(
      "derived:com.google.heart_rate.bpm:com.google.android.gms:merge_heart_rate_bpm",
      startTime,
      endTime
    );
  }

  /**
   * Gets heart rate for the signed in user during the given time period.
   * @param {Date} startTime the start time for the period to get weight
   * @param {Date} endTime the end time for the period to get weight
   */
  getHeartRateDaily(startTime, endTime) {
    return this.getAggregateData(
      "derived:com.google.heart_rate.bpm:com.google.android.gms:merge_heart_rate_bpm",
      startTime.getTime(),
      endTime.getTime()
    );
  }

  /**
   * @param {string} dataSourceId
   * @param {number} startTimeMs
   * @param {number} endTimeMs
   */
  async fetchAggregateData(dataSourceId, startTimeMs, endTimeMs) {
    const result = await this._fitness.dataset.aggregate({
      userId: "me",
      requestBody: {
        aggregateBy: [{ dataSourceId: dataSourceId }],
        bucketByTime: { durationMillis: this.ONE_DAY_MS },
        startTimeMillis: startTimeMs,
        endTimeMillis: endTimeMs,
      },
    });

    return result.data.bucket;
  }

  /**
   * @param {string} dataSourceId
   * @param {number} startTimeMs
   * @param {number} endTimeMs
   * @returns {Promise<import("googleapis").fitness_v1.Schema$AggregateBucket[]>}
   */
  async getAggregateData(dataSourceId, startTimeMs, endTimeMs) {
    const localEndTimeMs =
      endTimeMs - startTimeMs > this.NINETY_DAYS_MS
        ? startTimeMs + this.NINETY_DAYS_MS
        : endTimeMs;

    const firstPart = await this.fetchAggregateData(
      dataSourceId,
      startTimeMs,
      localEndTimeMs
    );

    return endTimeMs === localEndTimeMs
      ? firstPart
      : firstPart.concat(
          await this.getAggregateData(dataSourceId, localEndTimeMs, endTimeMs)
        );
  }

  /**
   * Gets a dataset via the Google Fit API. If successful it returns a
   * Users.dataSources.datasets resource.
   * @see {@link https://developers.google.com/fit/rest/v1/reference/users/dataSources/datasets Users.dataSources.datasets Resource}
   * @param {string} dataSource
   * @param {Date} startTime
   * @param {Date} endTime
   */
  async _getDatasets(dataSource, startTime, endTime) {
    // TODO: Implement paging using pageToken
    //       See: https://developers.google.com/fit/rest/v1/reference/users/dataSources/datasets/get

    // The Google Fit API takes timestamps in nanoseconds so we must convert milliseconds
    // returned by Date.getTime() to nanoseconds.
    const nanoSecondsPerMillisecond = 1000000;

    const result = await this._fitness.dataSources.datasets.get({
      userId: "me",
      datasetId:
        startTime.getTime() * nanoSecondsPerMillisecond +
        "-" +
        endTime.getTime() * nanoSecondsPerMillisecond,
      dataSourceId: dataSource,
    });

    return result.data;
  }
}

exports.default = GoogleFit;
