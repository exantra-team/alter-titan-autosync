const OAuth = require("oauth").OAuth;
const {
  default: AutoSyncProvider,
  AutoSyncError,
} = require("./AutoSyncProvider");

class GarminConnect extends AutoSyncProvider {
  constructor({ consumerKey, consumerSecret, redirectUri }) {
    super();

    const requestTokenUrl =
      "https://connectapi.garmin.com/oauth-service/oauth/request_token";
    const accessTokenUrl =
      "https://connectapi.garmin.com/oauth-service/oauth/access_token";

    this._redirectUri = redirectUri;

    const oauth = new OAuth(
      requestTokenUrl,
      accessTokenUrl,
      consumerKey,
      consumerSecret,
      "1.0",
      redirectUri,
      "HMAC-SHA1"
    );

    this._client = oauth;
  }

  getSyncMethod() {
    return "PUSH";
  }

  setup() {
    return new Promise((resolve, reject) => {
      const { GARMIN_CONSUMER_KEY, GARMIN_CONSUMER_SECRET } = process.env;
      const userAuthUrl = "https://connect.garmin.com/oauthConfirm";

      this._client.getOAuthRequestToken((err, token, tokenSecret, results) => {
        if (err) {
          return reject(err);
        }

        return resolve({
          url: `${userAuthUrl}?oauth_token=${token}&oauth_callback=${this._redirectUri}`,
          secret: tokenSecret,
        });
      });
    });
  }

  authCallback(search, secret) {
    return new Promise((resolve, reject) => {
      const urlParam = new URLSearchParams(search);
      this._client.getOAuthAccessToken(
        urlParam.get("oauth_token"),
        secret,
        urlParam.get("oauth_verifier"),
        (err, token, tokenSecret, results) => {
          if (err) {
            return reject(err);
          }

          return resolve(JSON.stringify({ token, tokenSecret }));
        }
      );
    });
  }

  /**
   * @param {[{
   * type: "workout" | "step",
   * startTime: number,
   * duration: number,
   * workoutType: string?,
   * calories: number?,
   * steps: number?
   * }]} data
   */
  syncFromPush(data) {
    try {
      const workoutData = data
        .filter((e) => e.type === "workout")
        .map(({ workoutType: type, startTime, duration, calories }) => ({
          type,
          startTime,
          duration,
          calories,
        }));

      const stepData = data.filter((e) => e.type === "step");

      const now = Date.now();

      return {
        workouts: {
          data: workoutData,
          endTime: workoutData.reduce((t, e) => {
            const endTime = e.startTime + e.duration;
            return endTime > t ? endTime : t;
          }, 0),
        },
        steps: {
          count: stepData.reduce((count, e) => count + e.steps, 0),
          endTime: stepData.reduce((t, e) => {
            const endTime = e.startTime + e.duration;
            return endTime > t ? endTime : t;
          }, 0),
        },
      };
    } catch (e) {
      switch (e.statusCode) {
        case 412:
          throw new AutoSyncError("REQUIRE_REAUTHORIZATION");
        default:
          throw e;
      }
    }
  }

  getPushId(authToken) {
    return JSON.parse(authToken).token;
  }

  deregister(authToken) {
    return new Promise((resolve, reject) => {
      const token = JSON.parse(authToken);
      this._client.delete(
        "https://apis.garmin.com/wellness-api/rest/user/registration",
        token.token,
        token.tokenSecret,
        (err, result, response) => {
          if (err) {
            return reject(err);
          }

          return resolve();
        }
      );
    });
  }
}

exports.default = GarminConnect;
exports.processPushRequest = (req) => {
  const data = req.body;
  const ret = {};
  for (const [key, value] of Object.entries(data)) {
    switch (key) {
      case "activities": {
        for (const entry of value) {
          const token = entry.userAccessToken;
          if (!ret[token]) ret[token] = {};
          if (!ret[token].data) ret[token].data = [];
          ret[token].data.push({
            type: "workout",
            workoutType: entry.activityType,
            startTime: entry.startTimeInSeconds * 1000,
            duration: entry.durationInSeconds * 1000,
            calories: entry.activeKilocalories,
          });
        }
        break;
      }
      case "epochs": {
        for (const entry of value) {
          const token = entry.userAccessToken;
          if (!ret[token]) ret[token] = {};
          if (!ret[token].data) ret[token].data = [];
          ret[token].data.push({
            type: "step",
            startTime: entry.startTimeInSeconds * 1000,
            duration: entry.durationInSeconds * 1000,
            steps: entry.steps,
          });
        }
        break;
      }
      case "deregistrations": {
        for (const entry of value) {
          const token = entry.userAccessToken;
          if (!ret[token]) ret[token] = {};
          ret[token].deactivated = true;
        }
        break;
      }
    }
  }

  return ret;
};
