const {
  default: AutoSyncProvider,
  AutoSyncError,
} = require("./AutoSyncProvider");
const {
  ClientCredentials,
  ResourceOwnerPassword,
  AuthorizationCode,
} = require("simple-oauth2");
const axios = require("axios").default;

class Fitbit extends AutoSyncProvider {
  constructor({ clientId, clientSecret, redirectUri }) {
    super();

    this._redirectUri = redirectUri;

    const config = {
      client: {
        id: clientId,
        secret: clientSecret,
      },
      auth: {
        authorizeHost: "https://www.fitbit.com",
        authorizePath: "/oauth2/authorize",
        tokenHost: "https://api.fitbit.com",
        tokenPath: "/oauth2/token",
      },
    };

    this._client = new AuthorizationCode(config);
  }

  getSyncMethod() {
    return "PULL";
  }

  setup() {
    const authorizationUri = this._client.authorizeURL({
      redirect_uri: this._redirectUri,
      scope: "activity",
      //Note: can use an anti-forgery token for 'state' to prevent XSS
      //https://dev.fitbit.com/build/reference/web-api/oauth2/#authorization-code-grant-flow
    });
    return { url: authorizationUri };
  }

  async authCallback(search) {
    const urlParams = new URLSearchParams(search);

    const authToken = urlParams.get("code");

    const tokenParams = {
      scope: "activity",
      code: authToken,
      redirect_uri: this._redirectUri,
    };

    const getTokenResponse = await this._client.getToken(tokenParams);
    this._accessToken = getTokenResponse.token.access_token;
    return getTokenResponse.token.refresh_token;
  }

  async postAuthSave() {
    const stepResponse = await axios.get(
      `https://api.fitbit.com/1/user/-/activities/steps/date/today/1d.json`,
      {
        headers: {
          Authorization: "Bearer " + this._accessToken,
        },
      }
    );

    if (stepResponse.data) {
      /** @type {[{dateTime: string, value: number}]} */
      const arr = stepResponse.data["activities-steps"];
      if (arr && arr.length > 0) {
        return JSON.stringify(arr[0]);
      }
    }

    return null;
  }

  async setAuthToken(authToken) {
    try {
      let accessToken = this._client.createToken({ refresh_token: authToken });

      accessToken = await accessToken.refresh();

      this._accessToken = accessToken.token.access_token;

      return accessToken.token.refresh_token;
    } catch (e) {
      if (e.data) {
        switch (e.data.res.statusCode) {
          case 400:
            throw new AutoSyncError("REQUIRE_REAUTHORIZATION");
        }
      }

      throw e;
    }
  }

  async sync({ workoutStartTime, stepStartTime, save }) {
    let config = {
      headers: {
        Authorization: "Bearer " + this._accessToken,
      },
    };

    let workoutData = [];
    let steps = {
      count: 0,
      endTime: 0,
    };

    /** @type {{dateTime: string, value: number}} */
    let saveObject = null;
    if (save) {
      try {
        saveObject = JSON.parse(save);
      } catch (e) {
        console.error("Error parsing Fitbit save: " + JSON.stringify(e));
      }
    }

    const now = Date.now();

    let next =
      "https://api.fitbit.com/1/user/-/activities/list.json" +
      "?afterDate=" +
      new Date(workoutStartTime).toISOString().replace(/\..+/, "") +
      "&sort=asc" +
      "&limit=20" +
      "&offset=0";

    let endTime = 0;

    while (next && next !== "") {
      // eslint-disable-next-line no-await-in-loop
      const response = await axios.get(next, config);
      const activities = response.data.activities;

      workoutData = workoutData.concat(
        activities
          .map(({ activityName, startTime, activeDuration, calories }) => {
            const startTimeMs = new Date(startTime).getTime();
            return {
              startTime: startTimeMs,
              duration: activeDuration,
              type: activityName,
              calories,
            };
          })
          .filter((e) => e)
      );

      endTime = activities.reduce((prev, { startTime, duration }) => {
        const endTimeMs =
          new Date(startTime.replace(/\..+/, "") + ".000Z").getTime() +
          duration;
        if (endTimeMs > prev) return endTimeMs;
        return prev;
      }, endTime);

      next = response.data.pagination.next;
    }

    const stepStartDate = saveObject
      ? saveObject.dateTime
      : new Date(stepStartTime - 24 * 60 * 60 * 1000)
          .toISOString()
          .replace(/T.+/, "");
    const stepEndDate = new Date(now + 24 * 60 * 60 * 1000)
      .toISOString()
      .replace(/T.+/, "");
    const stepResponse = await axios.get(
      `https://api.fitbit.com/1/user/-/activities/steps/date/${stepStartDate}/${stepEndDate}.json`,
      config
    );

    let newSave = save;
    if (stepResponse.data) {
      /** @type {[{dateTime: string, value: number}]} */
      const arr = stepResponse.data["activities-steps"];
      if (arr) {
        const nonZeros = arr
          .map(({ dateTime, value }) => ({
            dateTime,
            value: parseInt(value),
          }))
          .filter((e) => e.value > 0);

        const sessions = saveObject
          ? nonZeros.filter(
              (e) =>
                new Date(e.dateTime).getTime() >=
                new Date(saveObject.dateTime).getTime()
            )
          : nonZeros.slice(nonZeros.length - 1);

        if (sessions.length > 0) {
          let count = 0;
          for (const session of sessions) {
            count += session.value;
            if (saveObject && session.dateTime === saveObject.dateTime) {
              count -= saveObject.value;
            }
          }

          steps = {
            count,
            endTime:
              count > 0
                ? new Date(sessions[sessions.length - 1].dateTime).getTime()
                : 0,
          };

          newSave = JSON.stringify(sessions[sessions.length - 1]);
        }
      }
    }

    return {
      workouts: {
        data: workoutData,
        endTime,
      },
      steps,
      save: newSave,
    };
  }
}

exports.default = Fitbit;
