require("dotenv").config();
const express = require("express");
const {
  default: AutoSyncProvider,
  AutoSyncError,
} = require("./auto-sync-providers/AutoSyncProvider");
const { default: Template } = require("./auto-sync-providers/Template");
const { default: GoogleFit } = require("./auto-sync-providers/GoogleFit");
const { default: Fitbit } = require("./auto-sync-providers/Fitbit");
const {
  default: GarminConnect,
} = require("./auto-sync-providers/GarminConnect");
const open = require("open");
const readline = require("readline").createInterface(
  process.stdin,
  process.stdout
);
const fs = require("fs");

const AVAILABLE_PROVIDERS = ["Google Fit", "Fitbit", "Garmin Connect"];

/**
 * @param {string} name
 * @returns {AutoSyncProvider}
 */
function getAuthSyncProvider(name) {
  const callbackUri = "http://localhost:8000/auth-callback";
  switch (name) {
    case "Google Fit": {
      const { GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET } = process.env;

      return new GoogleFit({
        clientId: GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        redirectUri: callbackUri,
      });
    }
    case "Fitbit": {
      const { FITBIT_CLIENT_ID, FITBIT_CLIENT_SECRET } = process.env;

      return new Fitbit({
        clientId: FITBIT_CLIENT_ID,
        clientSecret: FITBIT_CLIENT_SECRET,
        redirectUri: callbackUri,
      });
    }
    case "Garmin Connect": {
      const { GARMIN_CONSUMER_KEY, GARMIN_CONSUMER_SECRET } = process.env;

      return new GarminConnect({
        consumerKey: GARMIN_CONSUMER_KEY,
        consumerSecret: GARMIN_CONSUMER_SECRET,
        redirectUri: callbackUri,
      });
    }
    case "Template":
      return new Template();
    default:
      throw new AutoSyncError("UNSUPPORTED");
  }
}

function startAuthListener() {
  return new Promise((resolve) => {
    const app = express();
    const server = app.listen(8000, () => {
      console.log("Auth callback server is listening at port 8000...");
    });

    app.get("/auth-callback", (req, res) => {
      resolve(req.query);
      res.send("OK");
      server.close();
      console.log("Auth callback server is closed.");
    });
  });
}

async function main(providerName) {
  try {
    const authTokensPath = "./authTokens.json";
    const savePath = "./save.json";
    const provider = getAuthSyncProvider(providerName);
    let savedTokens = {};
    let save = {};

    if (fs.existsSync(authTokensPath)) {
      savedTokens = JSON.parse(
        fs.readFileSync(authTokensPath, { encoding: "utf-8" })
      );
    }

    if (fs.existsSync(savePath)) {
      save = JSON.parse(fs.readFileSync(savePath, { encoding: "utf-8" }));
    }

    let token = null;
    if (savedTokens[providerName]) {
      token = savedTokens[providerName];
    } else {
      const setupResult = await provider.setup();
      if (!setupResult) {
        throw new Error("No result returned from setup method.");
      }

      console.log(`Proceed the authentication at ${setupResult.url}`);
      open(setupResult.url);
      const query = await startAuthListener();
      token = await provider.authCallback(query, setupResult.secret);
      savedTokens[providerName] = token;
      fs.writeFileSync(authTokensPath, JSON.stringify(savedTokens, null, 2));
      const postAuthSave = await provider.postAuthSave();
      if (postAuthSave) {
        save[providerName] = postAuthSave;
        fs.writeFileSync(savePath, JSON.stringify(save, null, 2));
      }
    }

    readline.question(
      `Please select an action[(p)Pull/(d)Deregister]: `,
      async (answer) => {
        readline.close();
        switch (answer.toLowerCase()) {
          case "p":
            try {
              const method = provider.getSyncMethod();
              switch (method) {
                case "PULL": {
                  const startTime =
                    new Date().getTime() - 2 * 24 * 60 * 60 * 1000;
                  const newToken = await provider.setAuthToken(token);
                  if (newToken) {
                    console.log("Got new token: " + newToken);
                    savedTokens[providerName] = newToken;
                    fs.writeFileSync(
                      authTokensPath,
                      JSON.stringify(savedTokens, null, 2)
                    );
                  }
                  const result = await provider.sync({
                    workoutStartTime: startTime,
                    stepStartTime: startTime,
                    save: save[providerName],
                  });
                  console.log(JSON.stringify(result, null, 2));
                  if (result.save) {
                    save[providerName] = result.save;
                    fs.writeFileSync(savePath, JSON.stringify(save, null, 2));
                  }
                  break;
                }
                case "PUSH": {
                  const result = await provider.syncFromPush(
                    save[providerName] || []
                  );
                  break;
                }
                default:
                  throw new Error("Sync method is not supported: " + method);
              }
            } catch (err) {
              console.error(err);
              if (err instanceof AutoSyncError) {
                if (err.code === "REQUIRE_REAUTHORIZATION") {
                  delete savedTokens[providerName];
                  fs.writeFileSync(
                    authTokensPath,
                    JSON.stringify(savedTokens, null, 2)
                  );
                }
              }
            }
            break;
          case "d":
            await provider.deregister(token);
            delete savedTokens[providerName];
            fs.writeFileSync(
              authTokensPath,
              JSON.stringify(savedTokens, null, 2)
            );
            console.log("Deregister successful.");
            break;
          default:
            throw new Error("Unknown action");
        }
      }
    );
  } catch (err) {
    console.error(err);
  }
}

for (let i = 0; i < AVAILABLE_PROVIDERS.length; i++) {
  console.log(`[${i}] ${AVAILABLE_PROVIDERS[i]}`);
}

readline.question(
  `Please enter a provider[0-${AVAILABLE_PROVIDERS.length - 1}]: `,
  (answer) => {
    main(AVAILABLE_PROVIDERS[answer]);
  }
);
